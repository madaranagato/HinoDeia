<html>
<head>
	
	<title><g:message code="springSecurity.login.title"/></title>
	<style type='text/css' media='screen'>
	
	h{
	color:#fff;
	}
	
	#login {
		margin: 15px 0px;
		padding: 0px;
		text-align: center;
		mergin-left: 80px;
		margin-left:800px;
	}

	#login .inner {
		
		position: relative;
		top: 200px;
		width: 390px;
		height: 390px;
		padding-bottom: 6px;
		margin: 60px auto;
		margin-top: -80px;
		
		text-align: left;
		
		background-color: #f0f0fa;
		-moz-box-shadow: 2px 2px 2px #eee;
		-webkit-box-shadow: 2px 2px 2px #eee;
		-khtml-box-shadow: 2px 2px 2px #eee;
		box-shadow: 2px 2px 2px #eee;
	}
	
	#logo_login{
	position:absolute;
	width:800px;
	height:500px;
	top:130px;
	left:35px;
	padding: 20px;
	color:#fff;
	
	}
	
	

	#login .inner .fheader {
		padding: 18px 26px 14px 26px;
		background-color: #f7f7ff;
		margin: 0px 0 14px 0;
		color: #2e3741;
		font-size: 30px;
		font-weight: bold;
	}

	#login .inner .cssform p {
		clear: left;
		margin: 0;
		padding: 4px 0 3px 0;
		padding-left: 105px;
		margin-bottom: 20px;
		height: 1%;
	}

	#login .inner .cssform input[type='text'] {
		width: 250px;
		margin-top: 25px;
	}
	
	#login .inner .cssform input[type='password'] {
		width: 250px;
		margin-top: 25px;
	}

	#login .inner .cssform label{
		font-weight: bold;
		float: left;
		text-align: right;
		margin-top: 30px;
		margin-left: -105px;
		width: 110px;
		padding-top: 3px;
		padding-right: 10px;
		font-size: 20px;
	}

	#login #remember_me_holder {
		padding-left: 120px;
	}

	#login #submit {
		margin-left: 180px;
		width: 80px;
		height:40px;
		text-align:center;
		padding: auto;
		curor:pointer;
		margin-top: 20px;
		color: #fff;
		font-width: bold;
		background-color:#666;
	}

	#login #submit:hover {
		background-color:#5b859a;
	}
	#login #remember_me_holder label {
		float: none;
		margin-left: 0;
		text-align: left;
		width: 200px;
		position: relative;
		top: 20px;
		font-size: 20px;
	}

	#login .inner .login_message {
		padding: 6px 25px 20px 25px;
		color: #c33;
	}

	#login .inner .text_ {
		width: 250px;
		height:40px;
	}

	#login .inner .chk {
		position:relative;
		top: 20px;
		height: 12px;
	}
	
	#cor{
		font-size:30px;
		color: #000;
	}
	
	#pos{
		position:relative;
		
		margin-left:245px;
		top: 40px;
		
		
	}
	#pos a{
	color:#000;
	text-decoration: none;
	
	}
	
	p{
	color: #000;
	
	}
	
	</style>
</head>

<body>

<div id='login'>

<div id="logo_login">

<asset:image src="rufino.png" height="200px" width="220"/>

	<div id="cor">
		Sistema de Controle de vendas
	</div>  
<p> Sistema voltado para vendas de perfumes, roupas e jóias com o objetivo de facilitar o controle do seu estoque.</p>
              
               


</div>


	<div class='inner'>
		<div class='fheader'><g:message code="springSecurity.login.header"/></div>

		<g:if test='${flash.message}'>
			<div class='login_message'>${flash.message}</div>
		</g:if>

		<form action='${postUrl}' method='POST' id='loginForm' class='cssform' autocomplete='off'>
			<p>
				<label  for='username'><g:message code="springSecurity.login.username.label"/>:</label>
				<input type='text' class='text_' name='j_username' id='username'/>
			</p>

			<p>
				<label for='password'><g:message code="springSecurity.login.password.label"/>:</label>
				<input type='password' class='text_' name='j_password' id='password'/>
			</p>

			<p id="remember_me_holder">
				<input type='checkbox' class='chk' name='${rememberMeParameter}' id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if>/>
				<label for='remember_me'><g:message code="springSecurity.login.remember.me.label"/></label>
			</p>

			<p>
				<input type='submit' id="submit" value='entrar'/>
			</p>
			
		</form>
		<div id="pos">
				<g:link controller="empreendedor" action="create">Cadastre-se agora!</g:link>
		</div>
	</div>
</div>
<script type='text/javascript'>
	<!--
	(function() {
		document.forms['loginForm'].elements['j_username'].focus();
	})();
	// -->
</script>
</body>
</html>
