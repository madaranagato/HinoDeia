import hinodeia.Pedidos
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_hinoDeia_pedidos_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/pedidos/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean: pedidosInstance, field: 'formaPagamento', 'error'))
printHtmlPart(1)
invokeTag('message','g',7,['code':("pedidos.formaPagamento.label"),'default':("Forma Pagamento")],-1)
printHtmlPart(2)
invokeTag('select','g',10,['name':("formaPagamento"),'from':(pedidosInstance.constraints.formaPagamento.inList),'required':(""),'value':(pedidosInstance?.formaPagamento),'valueMessagePrefix':("pedidos.formaPagamento")],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: pedidosInstance, field: 'dataVencimento', 'error'))
printHtmlPart(4)
invokeTag('message','g',16,['code':("pedidos.dataVencimento.label"),'default':("Data Vencimento")],-1)
printHtmlPart(2)
invokeTag('datePicker','g',19,['name':("dataVencimento"),'precision':("day"),'value':(pedidosInstance?.dataVencimento)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: pedidosInstance, field: 'cliente', 'error'))
printHtmlPart(5)
invokeTag('message','g',25,['code':("pedidos.cliente.label"),'default':("Cliente")],-1)
printHtmlPart(2)
invokeTag('select','g',28,['id':("cliente"),'name':("cliente.id"),'from':(hinodeia.Cliente.list()),'optionKey':("id"),'required':(""),'value':(pedidosInstance?.cliente?.id),'class':("many-to-one")],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: pedidosInstance, field: 'dataCompra', 'error'))
printHtmlPart(6)
invokeTag('message','g',34,['code':("pedidos.dataCompra.label"),'default':("Data Compra")],-1)
printHtmlPart(2)
invokeTag('datePicker','g',37,['name':("dataCompra"),'precision':("day"),'value':(pedidosInstance?.dataCompra)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: pedidosInstance, field: 'produtos', 'error'))
printHtmlPart(7)
invokeTag('message','g',43,['code':("pedidos.produtos.label"),'default':("Produtos")],-1)
printHtmlPart(8)
invokeTag('select','g',46,['name':("produtos"),'from':(hinodeia.Produto.list()),'multiple':("multiple"),'optionKey':("id"),'size':("5"),'value':(pedidosInstance?.produtos*.id),'class':("many-to-many")],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: pedidosInstance, field: 'quantidade', 'error'))
printHtmlPart(9)
invokeTag('message','g',52,['code':("pedidos.quantidade.label"),'default':("Quantidade")],-1)
printHtmlPart(2)
invokeTag('field','g',55,['name':("quantidade"),'type':("number"),'value':(pedidosInstance.quantidade),'required':("")],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: pedidosInstance, field: 'valorTotal', 'error'))
printHtmlPart(10)
invokeTag('message','g',61,['code':("pedidos.valorTotal.label"),'default':("Valor Total")],-1)
printHtmlPart(2)
invokeTag('field','g',64,['name':("valorTotal"),'value':(fieldValue(bean: pedidosInstance, field: 'valorTotal')),'required':("")],-1)
printHtmlPart(11)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1507508984213L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
