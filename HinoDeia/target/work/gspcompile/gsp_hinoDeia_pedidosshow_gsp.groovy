import hinodeia.Pedidos
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_hinoDeia_pedidosshow_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/pedidos/show.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',7,['var':("entityName"),'value':(message(code: 'pedidos.label', default: 'Pedidos'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',8,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',8,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',8,[:],2)
printHtmlPart(3)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
invokeTag('message','g',11,['code':("default.link.skip.label"),'default':("Skip to content&hellip;")],-1)
printHtmlPart(5)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(6)
invokeTag('message','g',14,['code':("default.home.label")],-1)
printHtmlPart(7)
createTagBody(2, {->
invokeTag('message','g',15,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('link','g',15,['class':("list"),'action':("index")],2)
printHtmlPart(8)
createTagBody(2, {->
invokeTag('message','g',16,['code':("default.new.label"),'args':([entityName])],-1)
})
invokeTag('link','g',16,['class':("create"),'action':("create")],2)
printHtmlPart(9)
invokeTag('message','g',20,['code':("default.show.label"),'args':([entityName])],-1)
printHtmlPart(10)
if(true && (flash.message)) {
printHtmlPart(11)
expressionOut.print(flash.message)
printHtmlPart(12)
}
printHtmlPart(13)
if(true && (pedidosInstance?.formaPagamento)) {
printHtmlPart(14)
invokeTag('message','g',28,['code':("pedidos.formaPagamento.label"),'default':("Forma Pagamento")],-1)
printHtmlPart(15)
invokeTag('fieldValue','g',30,['bean':(pedidosInstance),'field':("formaPagamento")],-1)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (pedidosInstance?.dataVencimento)) {
printHtmlPart(18)
invokeTag('message','g',37,['code':("pedidos.dataVencimento.label"),'default':("Data Vencimento")],-1)
printHtmlPart(19)
invokeTag('formatDate','g',39,['date':(pedidosInstance?.dataVencimento)],-1)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (pedidosInstance?.cliente)) {
printHtmlPart(20)
invokeTag('message','g',46,['code':("pedidos.cliente.label"),'default':("Cliente")],-1)
printHtmlPart(21)
createTagBody(3, {->
expressionOut.print(pedidosInstance?.cliente?.encodeAsHTML())
})
invokeTag('link','g',48,['controller':("cliente"),'action':("show"),'id':(pedidosInstance?.cliente?.id)],3)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (pedidosInstance?.dataCompra)) {
printHtmlPart(22)
invokeTag('message','g',55,['code':("pedidos.dataCompra.label"),'default':("Data Compra")],-1)
printHtmlPart(23)
invokeTag('formatDate','g',57,['date':(pedidosInstance?.dataCompra)],-1)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (pedidosInstance?.produtos)) {
printHtmlPart(24)
invokeTag('message','g',64,['code':("pedidos.produtos.label"),'default':("Produtos")],-1)
printHtmlPart(25)
for( p in (pedidosInstance.produtos) ) {
printHtmlPart(26)
createTagBody(4, {->
expressionOut.print(p?.encodeAsHTML())
})
invokeTag('link','g',67,['controller':("produto"),'action':("show"),'id':(p.id)],4)
printHtmlPart(27)
}
printHtmlPart(28)
}
printHtmlPart(17)
if(true && (pedidosInstance?.quantidade)) {
printHtmlPart(29)
invokeTag('message','g',75,['code':("pedidos.quantidade.label"),'default':("Quantidade")],-1)
printHtmlPart(30)
invokeTag('fieldValue','g',77,['bean':(pedidosInstance),'field':("quantidade")],-1)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (pedidosInstance?.valorTotal)) {
printHtmlPart(31)
invokeTag('message','g',84,['code':("pedidos.valorTotal.label"),'default':("Valor Total")],-1)
printHtmlPart(32)
invokeTag('fieldValue','g',86,['bean':(pedidosInstance),'field':("valorTotal")],-1)
printHtmlPart(16)
}
printHtmlPart(33)
createTagBody(2, {->
printHtmlPart(34)
createTagBody(3, {->
invokeTag('message','g',94,['code':("default.button.edit.label"),'default':("Edit")],-1)
})
invokeTag('link','g',94,['class':("edit"),'action':("edit"),'resource':(pedidosInstance)],3)
printHtmlPart(35)
invokeTag('actionSubmit','g',95,['class':("delete"),'action':("delete"),'value':(message(code: 'default.button.delete.label', default: 'Delete')),'onclick':("return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');")],-1)
printHtmlPart(36)
})
invokeTag('form','g',97,['url':([resource:pedidosInstance, action:'delete']),'method':("DELETE")],2)
printHtmlPart(37)
})
invokeTag('captureBody','sitemesh',99,[:],1)
printHtmlPart(38)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1507508984228L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
