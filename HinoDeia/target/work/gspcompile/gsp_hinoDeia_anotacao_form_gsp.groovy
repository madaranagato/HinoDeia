import hinodeia.Anotacao
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_hinoDeia_anotacao_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/anotacao/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean: anotacaoInstance, field: 'texto', 'error'))
printHtmlPart(1)
invokeTag('message','g',7,['code':("anotacao.texto.label"),'default':("Texto")],-1)
printHtmlPart(2)
invokeTag('textArea','g',10,['name':("texto"),'cols':("40"),'rows':("5"),'maxlength':("256"),'required':(""),'value':(anotacaoInstance?.texto)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: anotacaoInstance, field: 'empreendedor', 'error'))
printHtmlPart(4)
invokeTag('message','g',16,['code':("anotacao.empreendedor.label"),'default':("Empreendedor")],-1)
printHtmlPart(2)
invokeTag('select','g',19,['id':("empreendedor"),'name':("empreendedor.id"),'from':(hinodeia.Empreendedor.list()),'optionKey':("id"),'required':(""),'value':(anotacaoInstance?.empreendedor?.id),'class':("many-to-one")],-1)
printHtmlPart(5)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1508117821431L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
