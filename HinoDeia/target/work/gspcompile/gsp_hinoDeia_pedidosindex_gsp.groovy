import hinodeia.Pedidos
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_hinoDeia_pedidosindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/pedidos/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',7,['var':("entityName"),'value':(message(code: 'pedidos.label', default: 'Pedidos'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',8,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',8,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',8,[:],2)
printHtmlPart(3)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
invokeTag('message','g',12,['code':("default.link.skip.label"),'default':("Skip to content&hellip;")],-1)
printHtmlPart(5)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(6)
invokeTag('message','g',15,['code':("default.home.label")],-1)
printHtmlPart(7)
createTagBody(2, {->
invokeTag('message','g',16,['code':("Novo Pedido"),'args':([entityName])],-1)
})
invokeTag('link','g',16,['class':("create"),'action':("create")],2)
printHtmlPart(8)
createTagBody(2, {->
invokeTag('message','g',17,['code':("Pesquisar"),'args':([entityName])],-1)
})
invokeTag('link','g',17,['class':("busca_p"),'action':("pedidos")],2)
printHtmlPart(9)
if(true && (flash.message)) {
printHtmlPart(10)
expressionOut.print(flash.message)
printHtmlPart(11)
}
printHtmlPart(12)
invokeTag('sortableColumn','g',30,['property':("formaPagamento"),'title':(message(code: 'pedidos.formaPagamento.label', default: 'Forma Pagamento'))],-1)
printHtmlPart(13)
invokeTag('sortableColumn','g',32,['property':("dataVencimento"),'title':(message(code: 'pedidos.dataVencimento.label', default: 'Data Vencimento'))],-1)
printHtmlPart(14)
invokeTag('message','g',34,['code':("pedidos.cliente.label"),'default':("Cliente")],-1)
printHtmlPart(15)
invokeTag('sortableColumn','g',36,['property':("dataCompra"),'title':(message(code: 'pedidos.dataCompra.label', default: 'Data Compra'))],-1)
printHtmlPart(13)
invokeTag('sortableColumn','g',38,['property':("quantidade"),'title':(message(code: 'pedidos.quantidade.label', default: 'Quantidade'))],-1)
printHtmlPart(13)
invokeTag('sortableColumn','g',40,['property':("valorTotal"),'title':(message(code: 'pedidos.valorTotal.label', default: 'Valor Total'))],-1)
printHtmlPart(16)
loop:{
int i = 0
for( pedidosInstance in (pedidosInstanceList) ) {
printHtmlPart(17)
expressionOut.print((i % 2) == 0 ? 'even' : 'odd')
printHtmlPart(18)
createTagBody(3, {->
expressionOut.print(fieldValue(bean: pedidosInstance, field: "formaPagamento"))
})
invokeTag('link','g',48,['action':("show"),'id':(pedidosInstance.id)],3)
printHtmlPart(19)
invokeTag('formatDate','g',50,['date':(pedidosInstance.dataVencimento)],-1)
printHtmlPart(19)
expressionOut.print(fieldValue(bean: pedidosInstance, field: "cliente"))
printHtmlPart(19)
invokeTag('formatDate','g',54,['date':(pedidosInstance.dataCompra)],-1)
printHtmlPart(19)
expressionOut.print(fieldValue(bean: pedidosInstance, field: "quantidade"))
printHtmlPart(19)
expressionOut.print(fieldValue(bean: pedidosInstance, field: "valorTotal"))
printHtmlPart(20)
i++
}
}
printHtmlPart(21)
invokeTag('paginate','g',65,['total':(pedidosInstanceCount ?: 0)],-1)
printHtmlPart(22)
})
invokeTag('captureBody','sitemesh',68,[:],1)
printHtmlPart(23)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1511653162107L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
