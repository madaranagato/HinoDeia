import hinodeia.Empreendedor
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_hinoDeia_empreendedorshow_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/empreendedor/show.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',7,['var':("entityName"),'value':(message(code: 'empreendedor.label', default: 'Empreendedor'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',8,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',8,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',8,[:],2)
printHtmlPart(3)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
invokeTag('message','g',11,['code':("default.link.skip.label"),'default':("Skip to content&hellip;")],-1)
printHtmlPart(5)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(6)
invokeTag('message','g',14,['code':("default.home.label")],-1)
printHtmlPart(7)
createTagBody(2, {->
invokeTag('message','g',15,['code':("Listar"),'args':([entityName])],-1)
})
invokeTag('link','g',15,['class':("list_emp"),'action':("index")],2)
printHtmlPart(8)
createTagBody(2, {->
invokeTag('message','g',16,['code':("default.new.label"),'args':([entityName])],-1)
})
invokeTag('link','g',16,['class':("create_emp"),'action':("create")],2)
printHtmlPart(9)
invokeTag('message','g',20,['code':("default.show.label"),'args':([entityName])],-1)
printHtmlPart(10)
if(true && (flash.message)) {
printHtmlPart(11)
expressionOut.print(flash.message)
printHtmlPart(12)
}
printHtmlPart(13)
if(true && (empreendedorInstance?.username)) {
printHtmlPart(14)
invokeTag('message','g',28,['code':("empreendedor.username.label"),'default':("Username")],-1)
printHtmlPart(15)
invokeTag('fieldValue','g',30,['bean':(empreendedorInstance),'field':("username")],-1)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (empreendedorInstance?.password)) {
printHtmlPart(18)
invokeTag('message','g',37,['code':("empreendedor.password.label"),'default':("Password")],-1)
printHtmlPart(19)
invokeTag('fieldValue','g',39,['bean':(empreendedorInstance),'field':("password")],-1)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (empreendedorInstance?.nome)) {
printHtmlPart(20)
invokeTag('message','g',46,['code':("empreendedor.nome.label"),'default':("Nome")],-1)
printHtmlPart(21)
invokeTag('fieldValue','g',48,['bean':(empreendedorInstance),'field':("nome")],-1)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (empreendedorInstance?.email)) {
printHtmlPart(22)
invokeTag('message','g',55,['code':("empreendedor.email.label"),'default':("Email")],-1)
printHtmlPart(23)
invokeTag('fieldValue','g',57,['bean':(empreendedorInstance),'field':("email")],-1)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (empreendedorInstance?.accountExpired)) {
printHtmlPart(24)
invokeTag('message','g',64,['code':("empreendedor.accountExpired.label"),'default':("Account Expired")],-1)
printHtmlPart(25)
invokeTag('formatBoolean','g',66,['boolean':(empreendedorInstance?.accountExpired)],-1)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (empreendedorInstance?.accountLocked)) {
printHtmlPart(26)
invokeTag('message','g',73,['code':("empreendedor.accountLocked.label"),'default':("Account Locked")],-1)
printHtmlPart(27)
invokeTag('formatBoolean','g',75,['boolean':(empreendedorInstance?.accountLocked)],-1)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (empreendedorInstance?.anotacao)) {
printHtmlPart(28)
invokeTag('message','g',82,['code':("empreendedor.anotacao.label"),'default':("Anotacao")],-1)
printHtmlPart(29)
for( a in (empreendedorInstance.anotacao) ) {
printHtmlPart(30)
createTagBody(4, {->
expressionOut.print(a?.encodeAsHTML())
})
invokeTag('link','g',85,['controller':("anotacao"),'action':("show"),'id':(a.id)],4)
printHtmlPart(31)
}
printHtmlPart(32)
}
printHtmlPart(17)
if(true && (empreendedorInstance?.cliente)) {
printHtmlPart(33)
invokeTag('message','g',93,['code':("empreendedor.cliente.label"),'default':("Cliente")],-1)
printHtmlPart(29)
for( c in (empreendedorInstance.cliente) ) {
printHtmlPart(34)
createTagBody(4, {->
expressionOut.print(c?.encodeAsHTML())
})
invokeTag('link','g',96,['controller':("cliente"),'action':("show"),'id':(c.id)],4)
printHtmlPart(31)
}
printHtmlPart(32)
}
printHtmlPart(17)
if(true && (empreendedorInstance?.enabled)) {
printHtmlPart(35)
invokeTag('message','g',104,['code':("empreendedor.enabled.label"),'default':("Enabled")],-1)
printHtmlPart(36)
invokeTag('formatBoolean','g',106,['boolean':(empreendedorInstance?.enabled)],-1)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (empreendedorInstance?.passwordExpired)) {
printHtmlPart(37)
invokeTag('message','g',113,['code':("empreendedor.passwordExpired.label"),'default':("Password Expired")],-1)
printHtmlPart(38)
invokeTag('formatBoolean','g',115,['boolean':(empreendedorInstance?.passwordExpired)],-1)
printHtmlPart(16)
}
printHtmlPart(39)
createTagBody(2, {->
printHtmlPart(40)
createTagBody(3, {->
invokeTag('message','g',123,['code':("default.button.edit.label"),'default':("Edit")],-1)
})
invokeTag('link','g',123,['class':("edit"),'action':("edit"),'resource':(empreendedorInstance)],3)
printHtmlPart(41)
invokeTag('actionSubmit','g',124,['class':("delete"),'action':("delete"),'value':(message(code: 'default.button.delete.label', default: 'Delete')),'onclick':("return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');")],-1)
printHtmlPart(42)
})
invokeTag('form','g',126,['url':([resource:empreendedorInstance, action:'delete']),'method':("DELETE")],2)
printHtmlPart(43)
})
invokeTag('captureBody','sitemesh',128,[:],1)
printHtmlPart(44)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1508126052850L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
