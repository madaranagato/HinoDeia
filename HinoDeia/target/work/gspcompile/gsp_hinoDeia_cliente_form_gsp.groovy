import hinodeia.Cliente
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_hinoDeia_cliente_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/cliente/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean: clienteInstance, field: 'nome', 'error'))
printHtmlPart(1)
invokeTag('message','g',7,['code':("cliente.nome.label"),'default':("Nome")],-1)
printHtmlPart(2)
invokeTag('textField','g',10,['name':("nome"),'maxlength':("35"),'required':(""),'value':(clienteInstance?.nome)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: clienteInstance, field: 'email', 'error'))
printHtmlPart(4)
invokeTag('message','g',16,['code':("cliente.email.label"),'default':("Email")],-1)
printHtmlPart(2)
invokeTag('field','g',19,['type':("email"),'name':("email"),'required':(""),'value':(clienteInstance?.email)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: clienteInstance, field: 'sexo', 'error'))
printHtmlPart(5)
invokeTag('message','g',25,['code':("cliente.sexo.label"),'default':("Sexo")],-1)
printHtmlPart(2)
invokeTag('select','g',28,['name':("sexo"),'from':(clienteInstance.constraints.sexo.inList),'required':(""),'value':(clienteInstance?.sexo),'valueMessagePrefix':("cliente.sexo")],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: clienteInstance, field: 'empreendedor', 'error'))
printHtmlPart(6)
invokeTag('message','g',34,['code':("cliente.empreendedor.label"),'default':("Empreendedor")],-1)
printHtmlPart(2)
invokeTag('select','g',37,['id':("empreendedor"),'name':("empreendedor.id"),'from':(hinodeia.Empreendedor.list()),'optionKey':("id"),'required':(""),'value':(clienteInstance?.empreendedor?.id),'class':("many-to-one")],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: clienteInstance, field: 'endereco', 'error'))
printHtmlPart(7)
invokeTag('message','g',43,['code':("cliente.endereco.label"),'default':("Endereco")],-1)
printHtmlPart(2)
invokeTag('textField','g',46,['name':("endereco"),'required':(""),'value':(clienteInstance?.endereco)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: clienteInstance, field: 'telefone', 'error'))
printHtmlPart(8)
invokeTag('message','g',52,['code':("cliente.telefone.label"),'default':("Telefone")],-1)
printHtmlPart(2)
invokeTag('textField','g',55,['name':("telefone"),'required':(""),'value':(clienteInstance?.telefone)],-1)
printHtmlPart(9)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1508114983282L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
