import hinodeia.Lembrete
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_hinoDeia_lembrete_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/lembrete/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean: lembreteInstance, field: 'data', 'error'))
printHtmlPart(1)
invokeTag('message','g',7,['code':("lembrete.data.label"),'default':("Data")],-1)
printHtmlPart(2)
invokeTag('datePicker','g',10,['name':("data"),'precision':("day"),'value':(lembreteInstance?.data)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: lembreteInstance, field: 'pedidos', 'error'))
printHtmlPart(4)
invokeTag('message','g',16,['code':("lembrete.pedidos.label"),'default':("Pedidos")],-1)
printHtmlPart(2)
invokeTag('select','g',19,['id':("pedidos"),'name':("pedidos.id"),'from':(hinodeia.Pedidos.list()),'optionKey':("id"),'required':(""),'value':(lembreteInstance?.pedidos?.id),'class':("many-to-one")],-1)
printHtmlPart(5)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1507508984213L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
