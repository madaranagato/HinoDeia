import hinodeia.ProdutoEstoque
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_hinoDeia_produtoEstoque_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/produtoEstoque/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean: produtoEstoqueInstance, field: 'quantidade', 'error'))
printHtmlPart(1)
invokeTag('message','g',7,['code':("produtoEstoque.quantidade.label"),'default':("Quantidade")],-1)
printHtmlPart(2)
invokeTag('field','g',10,['name':("quantidade"),'type':("number"),'min':("0"),'value':(produtoEstoqueInstance.quantidade),'required':("")],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: produtoEstoqueInstance, field: 'empreendedor', 'error'))
printHtmlPart(4)
invokeTag('message','g',16,['code':("produtoEstoque.empreendedor.label"),'default':("Empreendedor")],-1)
printHtmlPart(2)
invokeTag('select','g',19,['id':("empreendedor"),'name':("empreendedor.id"),'from':(hinodeia.Empreendedor.list()),'optionKey':("id"),'required':(""),'value':(produtoEstoqueInstance?.empreendedor?.id),'class':("many-to-one")],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: produtoEstoqueInstance, field: 'nome', 'error'))
printHtmlPart(5)
invokeTag('message','g',25,['code':("produtoEstoque.nome.label"),'default':("Nome")],-1)
printHtmlPart(2)
invokeTag('textField','g',28,['name':("nome"),'required':(""),'value':(produtoEstoqueInstance?.nome)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: produtoEstoqueInstance, field: 'valor', 'error'))
printHtmlPart(6)
invokeTag('message','g',34,['code':("produtoEstoque.valor.label"),'default':("Valor")],-1)
printHtmlPart(2)
invokeTag('field','g',37,['name':("valor"),'value':(fieldValue(bean: produtoEstoqueInstance, field: 'valor')),'required':("")],-1)
printHtmlPart(7)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1507508984228L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
