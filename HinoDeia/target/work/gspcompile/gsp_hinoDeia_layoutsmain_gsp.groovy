import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_hinoDeia_layoutsmain_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/layouts/main.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',8,['gsp_sm_xmlClosingForEmptyTag':(""),'http-equiv':("Content-Type"),'content':("text/html; charset=UTF-8")],-1)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',9,['gsp_sm_xmlClosingForEmptyTag':(""),'http-equiv':("X-UA-Compatible"),'content':("IE=edge,chrome=1")],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('layoutTitle','g',10,['default':("Grails")],-1)
})
invokeTag('captureTitle','sitemesh',10,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',10,[:],2)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',11,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("viewport"),'content':("width=device-width, initial-scale=1.0")],-1)
printHtmlPart(2)
expressionOut.print(assetPath(src: 'icon.png'))
printHtmlPart(3)
invokeTag('stylesheet','asset',14,['src':("application.css")],-1)
printHtmlPart(1)
invokeTag('javascript','asset',15,['src':("application.js")],-1)
printHtmlPart(1)
invokeTag('layoutHead','g',16,[:],-1)
printHtmlPart(4)
})
invokeTag('captureHead','sitemesh',17,[:],1)
printHtmlPart(5)
createTagBody(1, {->
printHtmlPart(6)
createTagBody(2, {->
printHtmlPart(7)
createClosureForHtmlPart(8, 3)
invokeTag('link','g',41,['controller':("produto"),'action':("index"),'title':("Cadastro e Lista de Produtos")],3)
printHtmlPart(9)
createClosureForHtmlPart(10, 3)
invokeTag('link','g',43,['controller':("cliente"),'action':("index"),'title':("Cadastro e Lista de Clientes")],3)
printHtmlPart(11)
createClosureForHtmlPart(12, 3)
invokeTag('link','g',45,['controller':("pedidos"),'action':("index"),'title':("Montar Pedido e Lista de Pedidos")],3)
printHtmlPart(13)
createClosureForHtmlPart(14, 3)
invokeTag('link','g',47,['controller':("anotacao"),'action':("index"),'title':("Realise anotações referentes ao Cliente")],3)
printHtmlPart(13)
createClosureForHtmlPart(15, 3)
invokeTag('link','g',49,['controller':("empreendedor"),'action':("index"),'title':("Cadastrar Empreendedor")],3)
printHtmlPart(16)
createClosureForHtmlPart(17, 3)
invokeTag('link','g',51,['controller':("logout"),'action':("index")],3)
printHtmlPart(18)
})
invokeTag('ifLoggedIn','sec',53,[:],2)
printHtmlPart(19)
invokeTag('layoutBody','g',59,[:],-1)
printHtmlPart(20)
})
invokeTag('captureBody','sitemesh',72,[:],1)
printHtmlPart(21)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1511659659715L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
