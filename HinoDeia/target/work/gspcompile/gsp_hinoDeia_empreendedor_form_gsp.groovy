import hinodeia.Empreendedor
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_hinoDeia_empreendedor_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/empreendedor/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean: empreendedorInstance, field: 'username', 'error'))
printHtmlPart(1)
invokeTag('message','g',7,['code':("empreendedor.username.label"),'default':("Username")],-1)
printHtmlPart(2)
invokeTag('textField','g',10,['name':("username"),'required':(""),'value':(empreendedorInstance?.username)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: empreendedorInstance, field: 'password', 'error'))
printHtmlPart(4)
invokeTag('message','g',16,['code':("empreendedor.password.label"),'default':("Password")],-1)
printHtmlPart(2)
invokeTag('textField','g',19,['name':("password"),'required':(""),'value':(empreendedorInstance?.password)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: empreendedorInstance, field: 'nome', 'error'))
printHtmlPart(5)
invokeTag('message','g',25,['code':("empreendedor.nome.label"),'default':("Nome")],-1)
printHtmlPart(2)
invokeTag('textField','g',28,['name':("nome"),'maxlength':("35"),'required':(""),'value':(empreendedorInstance?.nome)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: empreendedorInstance, field: 'email', 'error'))
printHtmlPart(6)
invokeTag('message','g',34,['code':("empreendedor.email.label"),'default':("Email")],-1)
printHtmlPart(2)
invokeTag('field','g',37,['type':("email"),'name':("email"),'required':(""),'value':(empreendedorInstance?.email)],-1)
printHtmlPart(7)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1508113379511L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
