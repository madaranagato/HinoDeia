package hinodeia

class Produto {
    
    String nome
    double valor
    int quantidade
    
   Tipo tipo
    
    static constraints = {
        nome nullabel:false, blank: false
        //quantidade min: 0 
        //valor min: 0
    }
    
    String toString(){
        nome
    }
}
