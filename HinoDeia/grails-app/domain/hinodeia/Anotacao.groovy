package hinodeia

class Anotacao {
    
    String texto
    
    Empreendedor empreendedor
    
    static constraints = {
        texto maxSize: 256
    }
    
    String toString(){
        texto
    }
}
