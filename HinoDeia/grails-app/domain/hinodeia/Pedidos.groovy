package hinodeia

class Pedidos {
    
    double valorTotal
    String formaPagamento
    Date dataVencimento
    Date dataCompra
    int quantidade
   
    Cliente cliente
    
    //Lembrete lembrete
    static hasMany = [produtos:Produto]
    static belongsTo = Produto
    
    static constraints = {
        //valorTotal min: 0
        formaPagamento (inList:["A vista", "Parcelado"])
        dataVencimento (blank: true)
    }
    
    def beforeInsert(){
        dataCompra = new Date();
    }
}
