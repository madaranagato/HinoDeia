package hinodeia

class Empreendedor extends Usuario{

    String nome
    String email
    
    static hasMany = [anotacao:Anotacao, cliente:Cliente]
    
    static constraints = {
        nome nullabel:false, blank: false, maxSize:35, unique: true
        email email:true, unique:true
    }
    
    String toString(){
        nome
    }
}