package hinodeia

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import org.springframework.security.access.annotation.Secured
@Secured('isFullyAuthenticated()')

class SobreController {

    def index() {
       render(view:"../_sobre")
    }
}
