package hinodeia



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import org.springframework.security.access.annotation.Secured
@Secured('isFullyAuthenticated()')

@Transactional(readOnly = true)
class ProdutoEstoqueController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ProdutoEstoque.list(params), model:[produtoEstoqueInstanceCount: ProdutoEstoque.count()]
    }


    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'produtoEstoque.label', default: 'ProdutoEstoque'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
