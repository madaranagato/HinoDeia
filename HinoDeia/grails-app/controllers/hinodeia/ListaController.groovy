package hinodeia

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import org.springframework.security.access.annotation.Secured
@Secured('isFullyAuthenticated()')


class ListaController {

    def index() { 
      println("passou aqui - index Lista")
        
        def consulta = Cliente.findAll()
        
        def nome = Cliente.findByNome('Rufino')
        println "B = " + nome
        
        def consultaF = Cliente.executeQuery("Select distinct a from Cliente a")        
        print consultaF
        
        println("consulta = " +consulta.nome+ "=" +consulta.email)
        render (view:'index', model:[consultaNaView:consulta])
    }
    
    def resultadoAjax = {
        println " madara = "+params.nome
        def busca = Cliente.findByNomeLike("%${params.nome}%")
    
        if (busca){
            render(template:'resultado', model:[resultado:busca])
        }else{
            flash.message = "Elemento não encontrado!"
            render(template:'resultado', 
            model:[resultado:busca],
            method:'GET')
        }
                
    }
}
