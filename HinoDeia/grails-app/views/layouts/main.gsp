<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">	
		<link rel="icon" sizes="114x114" href="${assetPath(src: 'icon.png')}">
                
  		<asset:stylesheet src="application.css"/>
		<asset:javascript src="application.js"/>
                
                <style>

*{
	margin:0;
	padding:0;
}

header{
width:100%;
height:50px;
top:0;
left:0;
background-color:#5b859a;
position:fixed;

}

.menu-icon{
	position:fixed;
	font-size:40px;
	font-weight:bold;
	padding:5px;
        padding-top: 20px;
	width:40px;
	height:40px;
	text-align:center;
	
	color:#fff;
	curor:pointer;
	transition: all .4s;
	left:50px;
	top:0;
        
}

.menu-icon:hover{
	
	;
}

#chk{
	display: none;
	position: absolute;

	}
	
.menur{
	height:100%;
	position: fixed;
	background-color:#222;
	top:0;
	left:0;
	overflow:hidden;
	transition: all .2s;
	}
	
#principal{
	width:150px;
	left:-300px;
	
	}
	
.menur ul{
	list-style: none;
        position: relative;
	}


.menur ul li a{
	display: block;
	font-size:18px;
	font-family: 'Arial';
	padding:10px;
	border-bottom: solid 1px #000;
	color:#ccc;
	text-decoration: none;
	transition: all .2s; 
        
	
	
	}
	
.menur ul li span{
	float: right;
	padding-right:10px;
       
	
	
	}
	
.menur ul li a:hover{
	background-color:#5b859a;
	}
	
.voltar{
	margin-top:60px;
	background-color:#111;
	border-left:solid 5px #444;
	
	}

.bg{

}

#chk:checked ~.bg{


}

#chk:checked ~ #principal{
	transform: translateX(300px);


}

#produtos{
	width:px;
	left:-300px;

}

#produtos:target{
	transform: translateX(300px);
	box-shadow: 2px #fff;

}




</style>

		<g:layoutHead/>
                
                
	</head>
	
 <body>
     
     
     <div id="cab">
         <input type="checkbox" id="chk">

         <label for="chk" class="menu-icon">&#9776;</label>

<div class="bg"></div>

<nav class="menur" id="principal">

	<ul>
		<li><a href="" class="voltar">Voltar</a></li>
		<li><a href="index.gsp">Principal</a></li>
                <li><g:link controller="produto" action="index" title="Cadastro e Lista de Produtos"> Produtos</g:link></li>
		<li><g:link controller="cliente" action="index" title="Cadastro e Lista de Clientes">Cliente</g:link></li>
		<li><g:link controller="pedidos" action="index" title="Montar Pedido e Lista de Pedidos">Pedidos</g:link></li>
                <li><g:link controller="anotacao" action="index" title="Realise anotações referentes ao Cliente">Anotacoes</g:link></li>
                <li><g:link controller="empreendedor" action="index" title="Cadastrar Empreendedor">Empreendedor</g:link></li>
                <li><a href="#">Sobre</a></li>
                <li class="current"><g:link controller="logout" action="index" >Sair</g:link></li>
		
	</ul>
</nav>


<nav class="menur" id="produtos">
	<ul>
		<li><a href="#" class="voltar">Voltar</a></li>
		<li><a href="#">Roupas</a></li>
		<li><a href="#">Perfumes</a></li>
		<li><a href="#">Jóias</a></li>
	</ul>

</nav>

         
         <div class="texto">
             
          
         
         HinoDeia
         
         </div>
        </div>
        
                        <g:layoutBody/>

                     <div id="rodape">
                        Desenvolvedores: </br>
                      Antonio Rufino </br>
                      Hellen Soares</br>
                      Raimundo Nonato</br>
                      
                </div>
                   
	
                
  </body>
      
</html>
