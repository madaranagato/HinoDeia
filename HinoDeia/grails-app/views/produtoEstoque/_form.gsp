<%@ page import="hinodeia.ProdutoEstoque" %>



<div class="fieldcontain ${hasErrors(bean: produtoEstoqueInstance, field: 'quantidade', 'error')} required">
	<label for="quantidade">
		<g:message code="produtoEstoque.quantidade.label" default="Quantidade" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="quantidade" type="number" min="0" value="${produtoEstoqueInstance.quantidade}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: produtoEstoqueInstance, field: 'empreendedor', 'error')} required">
	<label for="empreendedor">
		<g:message code="produtoEstoque.empreendedor.label" default="Empreendedor" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="empreendedor" name="empreendedor.id" from="${hinodeia.Empreendedor.list()}" optionKey="id" required="" value="${produtoEstoqueInstance?.empreendedor?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: produtoEstoqueInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="produtoEstoque.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" required="" value="${produtoEstoqueInstance?.nome}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: produtoEstoqueInstance, field: 'valor', 'error')} required">
	<label for="valor">
		<g:message code="produtoEstoque.valor.label" default="Valor" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="valor" value="${fieldValue(bean: produtoEstoqueInstance, field: 'valor')}" required=""/>

</div>

