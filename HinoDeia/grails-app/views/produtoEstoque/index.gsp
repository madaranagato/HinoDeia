
<%@ page import="hinodeia.ProdutoEstoque" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'produtoEstoque.label', default: 'ProdutoEstoque')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-produtoEstoque" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-produtoEstoque" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="quantidade" title="${message(code: 'produtoEstoque.quantidade.label', default: 'Quantidade')}" />
					
						<th><g:message code="produtoEstoque.empreendedor.label" default="Empreendedor" /></th>
					
						<g:sortableColumn property="nome" title="${message(code: 'produtoEstoque.nome.label', default: 'Nome')}" />
					
						<g:sortableColumn property="valor" title="${message(code: 'produtoEstoque.valor.label', default: 'Valor')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${produtoEstoqueInstanceList}" status="i" var="produtoEstoqueInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${produtoEstoqueInstance.id}">${fieldValue(bean: produtoEstoqueInstance, field: "quantidade")}</g:link></td>
					
						<td>${fieldValue(bean: produtoEstoqueInstance, field: "empreendedor")}</td>
					
						<td>${fieldValue(bean: produtoEstoqueInstance, field: "nomejhsdgfj")}</td>
					
						<td>${fieldValue(bean: produtoEstoqueInstance, field: "valor")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${produtoEstoqueInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
