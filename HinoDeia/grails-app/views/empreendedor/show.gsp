
<%@ page import="hinodeia.Empreendedor" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'empreendedor.label', default: 'Empreendedor')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-empreendedor" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list_emp" action="index"><g:message code="Listar" args="[entityName]" /></g:link></li>
				<li><g:link class="create_emp" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-empreendedor" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list empreendedor">
			
				<g:if test="${empreendedorInstance?.username}">
				<li class="fieldcontain">
					<span id="username-label" class="property-label"><g:message code="empreendedor.username.label" default="Username" /></span>
					
						<span class="property-value" aria-labelledby="username-label"><g:fieldValue bean="${empreendedorInstance}" field="username"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empreendedorInstance?.password}">
				<li class="fieldcontain">
					<span id="password-label" class="property-label"><g:message code="empreendedor.password.label" default="Password" /></span>
					
						<span class="property-value" aria-labelledby="password-label"><g:fieldValue bean="${empreendedorInstance}" field="password"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empreendedorInstance?.nome}">
				<li class="fieldcontain">
					<span id="nome-label" class="property-label"><g:message code="empreendedor.nome.label" default="Nome" /></span>
					
						<span class="property-value" aria-labelledby="nome-label"><g:fieldValue bean="${empreendedorInstance}" field="nome"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empreendedorInstance?.email}">
				<li class="fieldcontain">
					<span id="email-label" class="property-label"><g:message code="empreendedor.email.label" default="Email" /></span>
					
						<span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${empreendedorInstance}" field="email"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${empreendedorInstance?.accountExpired}">
				<li class="fieldcontain">
					<span id="accountExpired-label" class="property-label"><g:message code="empreendedor.accountExpired.label" default="Account Expired" /></span>
					
						<span class="property-value" aria-labelledby="accountExpired-label"><g:formatBoolean boolean="${empreendedorInstance?.accountExpired}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${empreendedorInstance?.accountLocked}">
				<li class="fieldcontain">
					<span id="accountLocked-label" class="property-label"><g:message code="empreendedor.accountLocked.label" default="Account Locked" /></span>
					
						<span class="property-value" aria-labelledby="accountLocked-label"><g:formatBoolean boolean="${empreendedorInstance?.accountLocked}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${empreendedorInstance?.anotacao}">
				<li class="fieldcontain">
					<span id="anotacao-label" class="property-label"><g:message code="empreendedor.anotacao.label" default="Anotacao" /></span>
					
						<g:each in="${empreendedorInstance.anotacao}" var="a">
						<span class="property-value" aria-labelledby="anotacao-label"><g:link controller="anotacao" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${empreendedorInstance?.cliente}">
				<li class="fieldcontain">
					<span id="cliente-label" class="property-label"><g:message code="empreendedor.cliente.label" default="Cliente" /></span>
					
						<g:each in="${empreendedorInstance.cliente}" var="c">
						<span class="property-value" aria-labelledby="cliente-label"><g:link controller="cliente" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${empreendedorInstance?.enabled}">
				<li class="fieldcontain">
					<span id="enabled-label" class="property-label"><g:message code="empreendedor.enabled.label" default="Enabled" /></span>
					
						<span class="property-value" aria-labelledby="enabled-label"><g:formatBoolean boolean="${empreendedorInstance?.enabled}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${empreendedorInstance?.passwordExpired}">
				<li class="fieldcontain">
					<span id="passwordExpired-label" class="property-label"><g:message code="empreendedor.passwordExpired.label" default="Password Expired" /></span>
					
						<span class="property-value" aria-labelledby="passwordExpired-label"><g:formatBoolean boolean="${empreendedorInstance?.passwordExpired}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:empreendedorInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${empreendedorInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
