<%@ page import="hinodeia.Empreendedor" %>



<div class="fieldcontain ${hasErrors(bean: empreendedorInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="empreendedor.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" required="" value="${empreendedorInstance?.username}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: empreendedorInstance, field: 'password', 'error')} required">
	<label for="password">
		<g:message code="empreendedor.password.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="password" required="" value="${empreendedorInstance?.password}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: empreendedorInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="empreendedor.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" maxlength="35" required="" value="${empreendedorInstance?.nome}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: empreendedorInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="empreendedor.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="email" required="" value="${empreendedorInstance?.email}"/>

</div>
