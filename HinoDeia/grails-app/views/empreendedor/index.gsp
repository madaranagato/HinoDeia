
<%@ page import="hinodeia.Empreendedor" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'empreendedor.label', default: 'Empreendedor')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
            <div id="conteudo">
		<a href="#list-empreendedor" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create_emp" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-empreendedor" class="content scaffold-list" role="main">
			<h1><g:message code="Lista de empreendedores cadastrados" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="username" title="${message(code: 'empreendedor.username.label', default: 'Username')}" />
					
						<g:sortableColumn property="nome" title="${message(code: 'empreendedor.nome.label', default: 'Nome')}" />
					
						<g:sortableColumn property="email" title="${message(code: 'empreendedor.email.label', default: 'Email')}" />
					</tr>
				</thead>
				<tbody>
				<g:each in="${empreendedorInstanceList}" status="i" var="empreendedorInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${empreendedorInstance.id}">${fieldValue(bean: empreendedorInstance, field: "username")}</g:link></td>
					
						
					
						<td>${fieldValue(bean: empreendedorInstance, field: "nome")}</td>
					
						<td>${fieldValue(bean: empreendedorInstance, field: "email")}</td>
					
						
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${empreendedorInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
