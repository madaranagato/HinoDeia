<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->

<%@ page contentType="text/html;charset=UTF-8" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listando Clientes Cadastrados</title>
  <meta name="layout" content="main"/>
    </head>
    <body>
        <center><h1>Listando Clientes Cadastrados</h1></center> 
        <div class="nav" role="navigation">
        <ul>
            <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
	</ul>
        </div>
        <g:each in="${consultaNaView}" status="i" var="valor">
        <br/>
         <center> ${fieldValue(bean:valor,field:'nome')}<br/> 
          ${fieldValue(bean:valor, field:'email')} <br/>
         </center>
        </g:each>  
         <br> </br>
         <div class="nav">
            <center> <g:render template="busca"/> </center>  
         </div>    
         
         <div class="nav" id="resultado">
            <center> <g:render template="resultado"/> </center>  
         </div>    
    </body>