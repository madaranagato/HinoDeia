<%@ page import="hinodeia.Produto" %>



<div class="fieldform ${hasErrors(bean: produtoInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="produto.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" required="" value="${produtoInstance?.nome}"/>

</div>

<div class="fieldform ${hasErrors(bean: produtoInstance, field: 'quantidade', 'error')} required">
	<label for="quantidade">
		<g:message code="produto.quantidade.label" default="Quantidade" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="quantidade" type="number" value="${produtoInstance.quantidade}" required=""/>

</div>

<div class="fieldform ${hasErrors(bean: produtoInstance, field: 'tipo', 'error')} required">
	<label for="tipo">
		<g:message code="produto.tipo.label" default="Tipo" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="tipo" name="tipo.id" from="${hinodeia.Tipo.list()}" optionKey="id" required="" value="${produtoInstance?.tipo?.id}" class="many-to-one"/>

</div>

<div class="fieldform ${hasErrors(bean: produtoInstance, field: 'valor', 'error')} required">
	<label for="valor">
		<g:message code="produto.valor.label" default="Valor" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="valor" value="${fieldValue(bean: produtoInstance, field: 'valor')}" required=""/>

</div>

